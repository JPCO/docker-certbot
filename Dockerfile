FROM debian:buster

MAINTAINER  J.P.C. Oudeman

#
# Mounts: - /etc/letsencrypt
#         - /var/www
#

# Set noninteractive mode for apt-get 
ENV DEBIAN_FRONTEND=noninteractive \
 EMAIL=webmaster@example.loc \
 DOMAINS="www.example.loc"

# Install packages 
RUN apt-get update && \
apt-get install -y certbot && \
# Cleanup
apt-get clean && \
find /var/lib/apt/lists /tmp /var/tmp /var/log -type f -delete

WORKDIR /root
ADD assets/cli.ini /root

ENTRYPOINT ["/usr/bin/certbot"]
CMD ["renew"]
