#Certbot
---
Docker image for certbot. 
Two mappings relevant:

  - Webroot  
    /var/www
    
  - letsencrypt:
    /etc/letsencrypt

2 Environment variables I use to configure for my Docker Compose:  
    EMAIL
    DOMAINS
    
Run using docker-compose examples:
    docker-compose run certbot -c cli.ini  *//using environment variables*
    docker-compose run certbot renew --quiet

Default setting is to the renew command.